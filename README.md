# 勤怠打刻ツール
Teamsとジョブカンに同時に打刻するツール

# プラットフォーム
- Windows 10
- node.js v16

# 使い方
1. `.env`ファイルをこのREADMEと同じディレクトリに作成し、`TEAMS_URL={Teams打刻チャンネルのIncoming Webhook URL}`というテキストの行を追記する
2. この`dakoku`ディレクトリをDesktopに配置する
3. `node install`でライブラリをインストールする
4. `出勤.ps1`と`退勤.ps1`をDesktopにコピーする
5. 出勤/退勤時に `出勤.ps1`/`退勤.ps1`を起動する

# 注意
- dakokuで立ち上げるブラウザは、普段使いのものとは別のブラウザを使用してください
  - debugger portを開けないまま普段使用していると、このdakoku内のスクリプトからdebugger portを開けて使用できなくなります
- さらにEdgeをdakokuで使用する場合、以下に注意してください
  - 「スタートアップ ブースト」をOffにする
    - これがONになっているとEdgeを閉じてもbackgroundでEdgeが立ち上がるので、上述の不具合が生じます
    - ![設定](start-up.png)
    - c.f. https://answers.microsoft.com/en-us/microsoftedge/forum/all/microsoft-edge-still-running-in-background-after/4c8128b8-1c26-450b-b3ac-ab59baa34a08

# TODO
- i18n
- Mac対応
- 月末にジョブカン申請する機能