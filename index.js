require("dotenv").config();
const axios = require("axios");
const waitPort = require("wait-port");
const find = require("find-process");
const open = require("open");
const CDP = require("chrome-remote-interface");

TEAMS_URL = process.env.TEAMS_URL;
if (!TEAMS_URL) {
  throw Error("TEAMS_URL environment variable is not set! READ README!");
}
DEBUGGER_PORT = parseInt(process.env.DEBUGGER_PORT ?? "9222");
BROWSER_PROCESS_NAME = "msedge.exe";

(async () => {
  const browersAlreadyLaunched = await find("name", BROWSER_PROCESS_NAME);
  if (browersAlreadyLaunched.length) {
    throw Error(
      `There are browser processes ${BROWSER_PROCESS_NAME}, please delete all in advance.`
    );
  }

  await open("", {
    app: {
      name: open.apps.edge,
      arguments: [`--remote-debugging-port=${DEBUGGER_PORT}`],
    },
  });
  waitPort({
    host: "localhost",
    port: DEBUGGER_PORT,
  }).then(async (open) => {
    if (open) {
      const client = await CDP({ port: DEBUGGER_PORT });
      const { Network, Page, Runtime } = client;
      await Promise.all([Network.enable(), Page.enable(), Runtime.enable()]);
      // somehow it needs reload here
      await Page.navigate({ url: "https://ssl.jobcan.jp/jbcoauth/login" });
      await Page.loadEventFired();
      await Promise.all([Page.enable(), Runtime.enable()]);

      await Runtime.evaluate({
        expression: '$("a[href*=google]")[0].click()',
        awaitPromise: true,
      });

      await Page.loadEventFired();
      await Promise.all([Page.enable(), Runtime.enable()]);

      await Runtime.evaluate({
        expression: '$("#adit-button-push").click()',
        awaitPromise: true,
      });

      const browersToBeDeleted = await find("name", BROWSER_PROCESS_NAME);
      browersToBeDeleted.forEach((browser) => {
        try {
          process.kill(browser.pid);
        } catch {
          console.log(`${browser.pid} : not found`);
        }
      });
    } else {
      throw Error("remote debugger port is missing.");
    }
  });
})();

axios.post(TEAMS_URL, {
  text: parseInt(process.argv[2]) ? "退勤" : "出勤",
});
